package io;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Formatter;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Main {

  // copy filt to .bak file
  private static void cp(Path path) {
    System.out.println(path.getFileName());
    String backupFile = path.getFileName() + ".bak";
    try {
      Files.copy(path, path.resolveSibling(backupFile));
    } catch (IOException e) {
      System.err.printf("Error copying %s: %s%n", path, e);
    }
  }

  // grep for a string
  // closing file ourtselves
  private static void grep(Path path, String filter) {
    Scanner input = null;
    try {
      input = new Scanner(path);
      while (input.hasNextLine()) {
        String line = input.nextLine();
        if (line.contains(filter)) {
          System.out.println(line);
        }
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", path, e);
    } finally {
      if (input != null) {
        input.close();
      }
    }
  }

  // grep to other file
  // closing using try with resources
  private static void grep(Path path, String filter, String to) {
    try (Scanner input = new Scanner(path);
         Formatter output = new Formatter(to)
    ) {
      while (input.hasNextLine()) {
        String line = input.nextLine();
        if (line.contains(filter)) {
          output.format("%s%n", line);
        }
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", path, e);
    }
  }

  // copying a file with streams
  private static void copyWithStreams(File file, String to) {
    try (FileInputStream input = new FileInputStream(file);
         FileOutputStream output = new FileOutputStream(to)
    ) {
      int c = input.read();
      while (c != -1) {

        output.write(c);
        c = input.read();
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", file, e);
    }
  }

  private static void scrapePage(String site, String to) {
    URL url = null;
    try {
      url = new URL(site);
    } catch (MalformedURLException e) {
      System.err.printf("Unable to open page %s:%s%n", site, e);
      return;
    }
    try (InputStream input = url.openStream();
         FileOutputStream output = new FileOutputStream(to)
    ) {
      int c = input.read();
      while (c != -1) {

        output.write(c);
        c = input.read();
      }
    } catch (IOException e) {
      System.err.printf("Error copying page %s : %s%n", site, e);
    }
  }

  private static void copyWithBufferedStreams(File file, String to) {
    try (var input = new BufferedInputStream(new FileInputStream(file));
         var output = new BufferedOutputStream(new FileOutputStream(to));
    ) {
      int c = input.read();
      while (c != -1) {
        output.write(c);
        c = input.read();
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", file, e);
    }
  }

  private static void compress(File file, String to) {
    try (var input = new BufferedInputStream(new FileInputStream(file));
         var output = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(to+ ".zip")));
    ) {
      output.putNextEntry(new ZipEntry(to));
      int c = input.read();
      while (c != -1) {

        output.write(c);
        c = input.read();
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", file, e);
    }
  }


  private static void copyWithBufferedReader(File file, String to) {
    try (var input = new BufferedReader(new FileReader(file));
         var output = new PrintWriter(new BufferedWriter(new FileWriter(to)));
    ) {
      String c = input.readLine();
      while (c != null) {
        output.printf("%s%n",c);
        c = input.readLine();
      }
    } catch (IOException e) {
      System.err.printf("File %s not found: %s%n", file, e);
    }
  }

  public static void main(String[] args) {
    final String BASE_DIRECORY = "data/";
    Path path = Path.of(BASE_DIRECORY, "ronde.csv");
    File file = path.toFile();
    if (!file.exists()) {
      System.err.println(file + " does not exist");
      return;
    }
    cp(path);
    grep(path, "202");
    grep(path, "202", BASE_DIRECORY + "filter.csv");
    //copyWithStreams(file,BASE_DIRECORY + "streamcopy.csv");
    scrapePage("http://www.google.com", BASE_DIRECORY + "home.html");
    copyWithBufferedStreams(file, BASE_DIRECORY + "bufferedstreamcopy.csv");
    compress(file, BASE_DIRECORY + file.getName() );
    copyWithBufferedReader(file, BASE_DIRECORY + "bufferedreadercopy.csv");

  }


}
